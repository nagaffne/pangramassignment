# pangramAssignment

// A pangram is a sentence using every letter of the alphabet at least once. 
fun isPangram(s: String): Boolean {
    var flag = false
    var searchFlag = false
    var numCharacters = 0
    var alphabet = "abcdefghijklmnopqrstuvwxyz"
    //iterate through the alphabet
    //start at a look for a, if a there
    //set searchFlag to true
    //else if not yet found searchFLag is false
	for (letter in alphabet.indices){
        for(char in s.indices){
            //the second parameter in the equals function is to ignore case
            if(s[char].equals(alphabet[letter], true)){searchFlag = true}
            else{if(searchFlag != true){searchFlag = false}}
        }
        //if found increase count by 1
        if(searchFlag){numCharacters++}
        //reset searchFlag the iterate to next alphabet character
        searchFlag = false
    }
    //if the 26 letters of the alphabet are there then we found a panagram
    if(numCharacters == 26){flag = true}
    return flag
}
    
fun main() {
   // when this runs you should see an output of true and false
   val panagram = "the quick brown fox jumps over the lazy dog"
   val notPanagram = "a quick movement of the enemy will jeopardize five gunboats"
   //val t1 = "Quick zephyrs blow, vexing daft Jim."
   //val t2 = "Sphinx of black quartz, judge my vow."
   println(isPangram(panagram))
   println(isPangram(notPanagram))
   //println(isPangram(t1))
   //println(isPangram(t2))
}